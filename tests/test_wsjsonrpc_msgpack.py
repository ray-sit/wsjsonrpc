#!/usr/bin/env python

from __future__ import print_function

import time
import umsgpack

from twisted.internet import defer
from twisted.python import failure
from twisted.trial import unittest

from wsjsonrpc import exception
from wsjsonrpc import protocol
from wsjsonrpc import factory
from wsjsonrpc import msgpackcodec


class JsonRpcWebSocketFactoryTestHarness(factory.JsonRpcWebSocketFactoryMixin):
    pass


class JsonRpcProtocolTestHarness(protocol.JsonRpcProtocolMixin):
    def __init__(self, *args, **kwargs):
        super(JsonRpcProtocolTestHarness, self).__init__(*args, **kwargs)
        self.last_message = None
        self.factory = None
        self.set_codec(msgpackcodec)

    def sendMessage(self, message, isBinary=False):
        assert isinstance(message, bytes)
        self.last_message = message
        return message


class ProtocolTest(unittest.TestCase):
    def test_unpack(self):
        data = {"A": "a"}
        jsondata = umsgpack.packb(data)
        p = JsonRpcProtocolTestHarness()
        assert p.unpack(jsondata) == data

    def test_unpack_bad_data_00(self):
        data = {"A": "a"}
        jsondata = umsgpack.packb(data)
        fragment = jsondata[0:-2]
        with self.assertRaises(exception.JsonRpcParseError):
            p = JsonRpcProtocolTestHarness()
            p.unpack(fragment)

    def test_unpack_bad_data_01(self):
        """This only works with JSON."""
        return

    def test_unpack_bad_data_02(self):
        data = {"A": "a"}
        jsondata = umsgpack.packb(data)
        fragment = jsondata[0:-2]
        try:
            p = JsonRpcProtocolTestHarness()
            p.unpack(fragment)
        except exception.JsonRpcParseError as e:
            assert e.request_id == None

    def test_timeout_requests_00(self):
        """
        Add to our protocol a deferred with a 'timeout' that's already in the past.
        """
        timeout = time.time()
        df = defer.Deferred()
        key = 0
        p = JsonRpcProtocolTestHarness()
        p.deferreds[key] = (df, timeout)

        def errback(f):
            assert isinstance(f, failure.Failure)
            assert isinstance(f.value, exception.JsonRpcInternalError)

        df.addBoth(errback)

        """
        Timeout old requests, which should trigger the errback on our deferred.
        """
        p.timeoutOldRequests()
        return df

    def test_timeout_requests_01(self):
        """
        Add to our protocol a non-deferred object to test error handling.
        """
        timeout = time.time()
        df = "NOT A DEFERRED"
        key = 0
        p = JsonRpcProtocolTestHarness()
        p.deferreds[key] = (df, timeout)

        """
        Timeout old requests, which should do nothing.
        """
        result = p.timeoutOldRequests()
        assert result is None

    def test_timeout_requests_02(self):
        timeout = time.time() + 1000
        df = defer.Deferred()
        key = 0
        p = JsonRpcProtocolTestHarness()
        p.deferreds[key] = (df, timeout)
        p.timeoutOldRequests()
        assert df.called == False

    def test_batcher_00(self):
        p = JsonRpcProtocolTestHarness()
        with p.batchContext() as batch:
            batch.request("echo", ["VALUE ONE"])
            batch.request("echo", ["VALUE TWO"])

        assert umsgpack.unpackb(p.last_message, use_tuple=True, use_ordered_dict=True) == (
            {"jsonrpc": "2.0", "method": "echo", "id": 0, "params": ("VALUE ONE",)},
            {"jsonrpc": "2.0", "method": "echo", "id": 1, "params": ("VALUE TWO",)},
        )

    def test_batcher_01(self):
        p = JsonRpcProtocolTestHarness()
        with p.batchContext() as batch:
            batch.request("echo", ["VALUE ONE"])
            batch.request("echo", ["VALUE TWO"])
            batch.cancel()

        assert p.last_message is None

    def test_batcher_02(self):
        p = JsonRpcProtocolTestHarness()
        with p.batchContext() as batch:
            batch.request("echo", ["VALUE ONE"])
            batch.notify("echo", ["VALUE TWO"])

        assert umsgpack.unpackb(p.last_message, use_tuple=True, use_ordered_dict=True) == (
            {"jsonrpc": "2.0", "method": "echo", "id": 0, "params": ("VALUE ONE",)},
            {"jsonrpc": "2.0", "method": "echo", "params": ("VALUE TWO",)},
        )

    def test_handle_parse_error_00(self):
        """This only works with JSON."""
        return

    def test_handle_parse_error_01(self):
        p = JsonRpcProtocolTestHarness()
        e = Exception("test exception")
        p.handleParseError(e)
        assert umsgpack.unpackb(p.last_message, use_tuple=True, use_ordered_dict=True) == {
            "jsonrpc": "2.0",
            "error": {"code": -32700, "message": "Parse error"},
            "id": None,
        }

    def test_on_message_00(self):
        jsondata = (
            umsgpack.packb({"jsonrpc": "2.0", "method": "echo", "id": 0, "params": ["VALUE ONE"]})
        )
        f = JsonRpcWebSocketFactoryTestHarness(concurrency=1)
        p = JsonRpcProtocolTestHarness()
        p.factory = f
        f._protocols.add(p)

        df = p.onMessage(jsondata, False)
        assert umsgpack.unpackb(p.last_message) == {
            "jsonrpc": "2.0",
            "error": {"code": -32601, "message": "Method not found"},
            "id": 0,
        }
        f.timeout_loop.stop()
        return df

    def test_on_message_01(self):
        jsondata = (
            umsgpack.packb({"jsonrpc": "2.0", "method": "echo", "id": 0, "params": ["VALUE ONE"]})
        )
        f = JsonRpcWebSocketFactoryTestHarness(concurrency=-1)
        f.registerMethod("echo", lambda x, y: "OK")
        p = JsonRpcProtocolTestHarness()
        p.factory = f
        f._protocols.add(p)

        df = p.onMessage(jsondata, False)
        assert umsgpack.unpackb(p.last_message) == {"jsonrpc": "2.0", "result": "OK", "id": 0}
        f.timeout_loop.stop()
        return df

    def test_on_message_02(self):
        f = JsonRpcWebSocketFactoryTestHarness(concurrency="BAD")
        f.registerMethod("echo", lambda x, y: "OK")
        p = JsonRpcProtocolTestHarness()
        p.factory = f
        f._protocols.add(p)

        jsondata = umsgpack.packb([{"jsonrpc": "2.0", "method": "echo", "id": 0, "params": ["VALUE ONE"]}, {"jsonrpc": "2.0", "method": "echo", "params": ["VALUE TWO"]}])
        df = p.onMessage(jsondata, False)
        assert umsgpack.unpackb(p.last_message) == [
            {"jsonrpc": "2.0", "result": "OK", "id": 0}
        ]
        f.timeout_loop.stop()
        return df

    def test_on_message_03(self):
        jsondata = umsgpack.packb([{"jsonrpc": "2.0", "result": "OK", "id": 0}])
        f = JsonRpcWebSocketFactoryTestHarness()
        p = JsonRpcProtocolTestHarness()
        p.factory = f
        f._protocols.add(p)

        timeout = time.time() + 1000
        df = defer.Deferred()
        key = 0
        p.deferreds[key] = (df, timeout)

        df = p.onMessage(jsondata, False)
        assert p.last_message == None
        f.timeout_loop.stop()
        return df

    def test_on_message_04(self):
        jsondata = umsgpack.packb(
            {"jsonrpc": "2.0", "error": {"code": -32601, "message": "Method not found"}, "id": 0}
        )

        f = JsonRpcWebSocketFactoryTestHarness()
        p = JsonRpcProtocolTestHarness()
        p.factory = f
        f._protocols.add(p)

        timeout = time.time() + 1000
        df = defer.Deferred()
        key = 0
        p.deferreds[key] = (df, timeout)

        def check(result):
            assert isinstance(result.value, exception.JsonRpcMethodNotFound)

        p.onMessage(jsondata, False)
        assert p.last_message == None
        f.timeout_loop.stop()
        df.addBoth(check)
        return df

    def test_on_message_05(self):
        jsondata = umsgpack.packb({"jsonrpc": "2.0", "method": "echo", "params": ["VALUE ONE"]})
        f = JsonRpcWebSocketFactoryTestHarness()
        f.registerMethod("echo", lambda x, y: "OK")
        p = JsonRpcProtocolTestHarness()
        p.factory = f
        f._protocols.add(p)

        df = p.onMessage(jsondata, False)
        assert p.last_message == None
        f.timeout_loop.stop()
        return df

    def test_on_message_06(self):
        jsondata = umsgpack.packb({"jsonrpc": "2.0", "result": "OK", "id": 0})
        f = JsonRpcWebSocketFactoryTestHarness()
        p = JsonRpcProtocolTestHarness()
        p.factory = f
        f._protocols.add(p)

        timeout = time.time() + 1000
        df = defer.Deferred()
        key = 0
        p.deferreds[key] = (df, timeout)

        df = p.onMessage(jsondata, False)
        assert p.last_message == None
        f.timeout_loop.stop()
        return df

    def test_on_message_07(self):
        jsondata = umsgpack.packb({"id":12, "bad":1})

        f = JsonRpcWebSocketFactoryTestHarness()
        p = JsonRpcProtocolTestHarness()
        p.factory = f
        f._protocols.add(p)

        timeout = time.time() + 1000
        df = defer.Deferred()
        key = 0
        p.deferreds[key] = (df, timeout)

        df = p.onMessage(jsondata, False)
        assert umsgpack.unpackb(p.last_message) == {
            "jsonrpc": "2.0",
            "error": {"code": -32600, "message": "Invalid Request"},
            "id": 12,
        }
        f.timeout_loop.stop()
        return df

    def test_request(self):
        p = JsonRpcProtocolTestHarness()
        p.request("echo", {"A0": "VALUE ONE"})

        assert umsgpack.unpackb(p.last_message) == {
            "jsonrpc": "2.0",
            "method": "echo",
            "id": 0,
            "params": {"A0": "VALUE ONE"},
        }

    def test_notify(self):
        p = JsonRpcProtocolTestHarness()
        p.notify("echo", ["VALUE ONE"])

        assert umsgpack.unpackb(p.last_message) == {
            "jsonrpc": "2.0",
            "method": "echo",
            "params": ["VALUE ONE"],
        }

    def test_send_none(self):
        p = JsonRpcProtocolTestHarness()
        p.sendObject(None)

        assert p.last_message == None

    def test_bad_params_request(self):
        """According to the spec, params MUST be a list or a dict."""
        p = JsonRpcProtocolTestHarness()
        with self.assertRaises(exception.JsonRpcInvalidParams):
            p.request("echo", 100)

    def test_bad_params_notify(self):
        """According to the spec, params MUST be a list or a dict."""
        p = JsonRpcProtocolTestHarness()
        rval = p.onNotification(method="echo", params=0, id=None)
        assert rval is None

    def test_bad_params_notify_mm(self):
        """According to the spec, params MUST be a list or a dict."""
        f = JsonRpcWebSocketFactoryTestHarness()
        f.registerMethod("echo", lambda x, y: y)
        p = JsonRpcProtocolTestHarness()
        p.factory = f
        f._protocols.add(p)

        rval = p.onNotification(method="hecho", params=0, id=None)
        assert rval is None

        f.timeout_loop.stop()

    def test_bad_factory_notify(self):
        """According to the spec, params MUST be a list or a dict."""

        class BadFactoryStub(object):
            def __init__(self):
                self.registry = {"echo": self.echo}

            def echo(self, proto, arg):
                return "OK"

        f = BadFactoryStub()
        p = JsonRpcProtocolTestHarness()
        p.factory = f

        df = p.onRequest(jsonrpc=None, method="echo", params=["one"], id=None)
        assert isinstance(df, defer.Deferred)

    def test_on_bad_notification_00(self):
        jsondata = umsgpack.packb({"jsonrpc": "2.0", "method": "echo", "params": ["VALUE ONE"]})

        def echo(proto, args):
            raise exception.JsonRpcException(id=None)

        f = JsonRpcWebSocketFactoryTestHarness()
        f.registerMethod("echo", echo)
        p = JsonRpcProtocolTestHarness()
        p.factory = f
        f._protocols.add(p)

        timeout = time.time() + 1000
        df = defer.Deferred()
        key = 0
        p.deferreds[key] = (df, timeout)

        df = p.onMessage(jsondata, False)
        assert p.last_message == None
        f.timeout_loop.stop()
        return df

    def test_on_bad_notification_01(self):
        jsondata = umsgpack.packb({"jsonrpc": "2.0", "method": "echo", "params": ["VALUE ONE"]})

        def echo(proto, args):
            raise Exception()

        f = JsonRpcWebSocketFactoryTestHarness()
        f.registerMethod("echo", echo)
        p = JsonRpcProtocolTestHarness()
        p.factory = f
        f._protocols.add(p)

        timeout = time.time() + 1000
        df = defer.Deferred()
        key = 0
        p.deferreds[key] = (df, timeout)

        df = p.onMessage(jsondata, False)
        assert p.last_message == None
        f.timeout_loop.stop()
        return df

    def test_on_bad_notification_02(self):
        jsondata = umsgpack.packb([{"jsonrpc": "2.0", "id": 2}])

        f = JsonRpcWebSocketFactoryTestHarness()
        p = JsonRpcProtocolTestHarness()
        p.factory = f
        f._protocols.add(p)

        timeout = time.time() + 1000
        df = defer.Deferred()
        key = 0
        p.deferreds[key] = (df, timeout)

        df = p.onMessage(jsondata, False)
        assert umsgpack.unpackb(p.last_message) == {
            "jsonrpc": "2.0",
            "error": {"code": -32600, "message": "Invalid Request"},
            "id": None,
        }
        f.timeout_loop.stop()
        return df

    def test_on_error_batch(self):
        jsondata = umsgpack.packb(
            [{"jsonrpc": "2.0", "error": {"code": -32700, "message": "Parse error"}, "id": 0}]
        )

        f = JsonRpcWebSocketFactoryTestHarness()
        f.beginLooping()
        f.registerMethod("echo", lambda x, y: y)
        p = JsonRpcProtocolTestHarness()
        p.factory = f
        f._protocols.add(p)

        timeout = time.time() + 1000
        df = defer.Deferred()
        key = 0
        p.deferreds[key] = (df, timeout)

        def check(result):
            assert isinstance(result.value, exception.JsonRpcParseError)

        df.addBoth(check)
        p.onMessage(jsondata, False)
        assert p.last_message == None
        f.timeout_loop.stop()
        return df

    def test_repackage_exception(self):

        p = JsonRpcProtocolTestHarness()
        envelope = p.packageExceptionObject(Exception(), 1)
        assert envelope == {
            "jsonrpc": "2.0",
            "error": {"code": -32603, "message": "Internal error"},
            "id": 1,
        }

    def test_timeout_factory(self):

        f = JsonRpcWebSocketFactoryTestHarness()
        f.beginLooping()
        f.registerMethod("echo", lambda x, y: y)

        p = JsonRpcProtocolTestHarness()
        df = p.request("echo", ["ONE"], timeout=0)
        p.factory = f
        f._protocols.add(p)
        f.timeoutOldRequests()
        f.timeout_loop.stop()

        def check(result):
            assert isinstance(result.value, exception.JsonRpcInternalError)
            return

        df.addBoth(check)
        return df
