#!/usr/bin/env python

import os
import sys

from twisted import logger
from twisted.internet import reactor
from twisted.internet import task

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from wsjsonrpc import factory

logobserver = logger.textFileLogObserver(sys.stdout)
#logobserver = logger.jsonFileLogObserver(sys.stdout, recordSeparator="")
logger.globalLogPublisher.addObserver(logobserver)


class ExportedAPI(object):
    """A class with an API that we want to export to remote websocket-jsonrpc clients."""

    logger = logger.Logger()

    def show_result(self, result):
        self.logger.debug("Received: {}".format(result))
        return "server received: {}".format(result)

    def rx_sum(self, protocol, a, b):
        """An exported method that returns a value directly."""
        self.logger.debug("rx_sum: {} {}".format(a, b))
        self.tx_echo(protocol, "REMOTE CALL FROM RX_SUM METHOD")
        return a + b

    def rx_echo(self, protocol, value):
        """An exported method that returns a deferred that fires after 1 second."""
        self.logger.debug("rx_echo: {}".format(value))

        return task.deferLater(
            reactor, 1, self.show_result, value
        )

    def tx_echo(self, protocol, value):
        """
        This calls the remote 'echo' method on a peer, given its protocol instance.

        The client might well have gone away by the time this is called.
        """
        self.logger.debug("tx_echo")
        return protocol.request("echo", ["please echo: {}".format(value)])

    def attach(self, factory):
        """Register any method beginning with rx_ as an API method."""
        for key in dir(self):
            if key[0:3] != "rx_":
                continue
            factory.registerMethod(key[3:], getattr(self, key))


if __name__ == "__main__":

    api = ExportedAPI()

    factory = factory.JsonRpcWebSocketServerFactory(u"ws://127.0.0.1:8095/wsjsonrpc")
    api.attach(factory)

    reactor.listenTCP(8095, factory)
    reactor.run()
